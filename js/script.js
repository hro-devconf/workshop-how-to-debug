(function(){
	//a self invoking hell

	class Point {
		constructor(x, y) {
			this.x = x;
			this.y = y;
		}
	}

	class Line {
		constructor(a, b){
			this.a = a;
			this.b = b;
		}

		/*static intersect(ln1, ln2){
			//check if two lines intersect
			let m1 = (ln1.a.y - ln1.b.y) / (ln1.a.x - ln1.b.x);
			let m2 = (ln2.a.y - ln2.b.y) / (ln2.a.x - ln2.b.x);

			//consider cases when slope becomes infinite
			let b1 = ln1.a.y - m1*ln1.a.x;
			let b2 = ln2.a.y - m2*ln2.a.x;

			let x = (b2 - b1) / (m2 - m1);
			let y = m1*x + b1;

			//now check if x and y are less than the end points
			return ((x <= Math.max(ln1.a.x, ln1.b.x)) && (x >= Math.min(ln1.a.x, ln1.b.x))
					&& (x <= Math.max(ln1.a.x, ln1.b.x)) && (x >= Math.min(ln1.a.x, ln1.b.x))
					&& (x <= Math.max(ln1.a.x, ln1.b.x)) && (x >= Math.min(ln1.a.x, ln1.b.x))
					&& (x <= Math.max(ln1.a.x, ln1.b.x)) && (x >= Math.min(ln1.a.x, ln1.b.x)));
		}*/
		// Check the direction these three points rotate 
		static RotationDirection(p1, p2, p3) {
			if (((p3.y - p1.y) * (p2.x - p1.x)) > ((p2.y - p1.y) * (p3.x - p1.x)))
				return 1;
			else if (((p3.y - p1.y) * (p2.x - p1.x)) == ((p2.y - p1.y) * (p3.x - p1.x)))
				return 0;
			return -1;
		}

		// perfect
		// works like a charm

		static intersect(ln1, ln2){

			var face1CounterClockwise = Line.RotationDirection(ln1.a, ln1.b, ln2.b);
			var face2CounterClockwise = Line.RotationDirection(ln1.a, ln1.b, ln2.a);
			var face3CounterClockwise = Line.RotationDirection(ln1.a, ln2.a, ln2.b);
			var face4CounterClockwise = Line.RotationDirection(ln1.b, ln2.a, ln2.b);

			// If face 1 and face 2 rotate different directions and face 3 and face 4 rotate different directions, 
			// then the lines intersect.
			var intersect = face1CounterClockwise != face2CounterClockwise && face3CounterClockwise != face4CounterClockwise;

			// If lines are on top of each other.
			if (face1CounterClockwise == 0 && face2CounterClockwise == 0 && face3CounterClockwise == 0 && face4CounterClockwise == 0)
				intersect = true;
			return intersect;
		}
	}

	var createPolygon = function(){
		var aPolygon = [];

		var points = [[200,100], [100,200], [200,300], [300,200], [200,100]];
		for( var i = 0; i < points.length; i++ ){
			aPolygon.push(new Point(points[i][0], points[i][1]));
		}

		return aPolygon;
	}

	//


	/*
	 * given a polygon tells if the point
	 * is inside a polygon or not
	 */
	var inside = function(aPolygon, aPoint){
		//first find a point that is outside
		//check if the point 

		//easy check first

		let highX = aPolygon[0].x;
		let lowX = aPolygon[0].x;
		let highY = aPolygon[0].y;
		let lowY = aPolygon[0].y;

		for(var i = 1; i < aPolygon.length; i++){
			highX = Math.max(aPolygon[i].x, highX);
			lowX = Math.min(aPolygon[i].x, lowX)
			highY = Math.max(aPolygon[i].y, highY);
			lowY = Math.min(aPolygon[i].y, lowY)
		}

		if(aPoint.x < lowX || aPoint.y < lowY || aPoint.x > highX || aPoint.y > highY)
			return false;

		//main check
		let strike = new Line(aPoint, new Point(highX + 1, aPoint.y));
		let count = 0;
		for(var i = 1; i < aPolygon.length; i++){
			let l = new Line(aPolygon[i-1], aPolygon[i]);
			if(Line.intersect(strike, l)){
				count++;
			}
		}
		//console.log(count);
		return ((count%2) !== 0);
	}

	var canvas=document.getElementById("canvas-wrap")
	var c2 = canvas.getContext('2d');

	var getMousePos = function(evt) {
		var rect = canvas.getBoundingClientRect();
		let point = new Point(evt.clientX - rect.left, evt.clientY - rect.top);
		return point;
	}

	var drawPolygon = function(aPolygon){
		//draws the polygon
		c2.fillStyle = '#f00';
		c2.beginPath();
		c2.moveTo(aPolygon[0].x, aPolygon[0].y);
		for(var i = 1; i < aPolygon.length - 1; i++){
			c2.lineTo(aPolygon[i].x, aPolygon[i].y);
		}
		c2.closePath();
		c2.fill();
	}

	let aPolygon = createPolygon();
	inside(aPolygon, new Point(200,200));
	drawPolygon(aPolygon);

	document.addEventListener("mousemove", function(evt){
		//document.getElementById("demo").innerHTML = "Hello World";
		aPoint = getMousePos(evt);
		if(inside(aPolygon, aPoint)){
			//do some here.
			c2.fillStyle = '#000';
			c2.fill();
		}else {
			c2.fillStyle = '#f00';
			c2.fill();
		}
	});

})();